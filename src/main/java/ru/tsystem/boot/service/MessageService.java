package ru.tsystem.boot.service;

import org.springframework.stereotype.Service;
import ru.tsystem.boot.config.PersonConfig;
import ru.tsystem.boot.model.MessageWrapper;

import java.util.stream.Collectors;

@Service
public class MessageService {
    private final "?" message;
    private final PersonConfig personConfig;

    public MessageService("?" message, PersonConfig personConfig) {
        this.message = message;
        this.personConfig = personConfig;
    }

    public "?" getMessages() {
        return message.stream()
                      .map(val -> new MessageWrapper(val.getMessage() + " " + personConfig.getName()))
                      .collect(Collectors.toList());
    }
}
