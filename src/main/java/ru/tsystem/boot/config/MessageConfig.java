package ru.tsystem.boot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tsystem.boot.model.MessageWrapper;

@Configuration
public class MessageConfig {

    @Bean
    public MessageWrapper getHelloMessage() {
        return new MessageWrapper("Hello World");
    }

    @Bean
    public MessageWrapper getTSystemMessage() {
        return new MessageWrapper("Hello T-Systems");
    }
}
